# Slides for SWT tutorial
### Usage
Requires `make` and `latexmk` to be installed.

Change into the directory of the tutorial slides you want to build and run
the makefile:
```
cd tut-01-orga-c
make -B -j8 -f ../Makefile
```

For automatic rebuild on file changes run the `dev` target:
```
make -f ../Makefile dev
```

### Quiz Generation
Go to the quiz folder. The topics in the question bank in `res/Multiple_Choice.txt` can be turned to LaTex-Slides. The resulting folder can then be moved to the `modules` folder. Usage:
```
python3 create.py TOPIC n
```
where TOPIC is the Headline from `res/Multiple_Choice.txt` and n is the maximum number of questions.

