#!/bin/sh

do_compile() {
    t=$1
    base=$(basename "$t")
    echo "[$t] Building $t"
    cd $t
    make -f ../Makefile clean > /dev/null
    make -f ../Makefile > /dev/null
    mv "build/tut.pdf" "../collection/$base.pdf"
    cd ..
    echo "[$t] Done with $t"
}

TUTS=$(ls | grep -ohP "tut-[0-9].*")

rm -rf collection
mkdir collection


for t in $TUTS; do
    do_compile $t &
done

wait

zip swt-tutorium.zip collection/*
