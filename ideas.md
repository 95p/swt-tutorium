# Ideen für Elemente

## Tutorium zu Git
[CCC Vortrag zu Git](https://github.com/fhars/divoc-git-talk)

## Tutorium zu CFGs
Um die Relevanz von Kontrollflussgraphen etc. auf anderen Feldern aufzuzeigen:
Exkurs Ghidra, basic building blocks, coverage based fuzzing, ... 

## Tutorium zu Parallelität
Hinweis auf [Deadlock Empire](https://deadlockempire.github.io/)
