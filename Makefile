PRES_NAME	:= tut
LATEX_COMMON_FLAGS	:= -halt-on-error -shell-escape -pdf -g -f

default: prep pres
all: prep pres

prep:	svg uml lst

pres:	prep build/$(PRES_NAME).pdf

dev: CONT_FLAG = -pvc
dev: latex-force prep pres


svg: $(patsubst %.svg,build/%.pdf,$(wildcard images/*.svg))
uml: $(patsubst ../uml-source/%.puml,../build/uml/%.pdf,$(wildcard ../uml-source/*.puml))
lst: $(patsubst %.tex,build/%.pdf,$(wildcard listings/*.tex))

build/images/%.pdf: images/%.svg
	mkdir -p build/images
	inkscape -D -z --file=$< --export-pdf=$@ --export-latex

../build/uml/%.pdf: ../uml-source/%.puml
	mkdir -p ../build/uml
	plantuml -tpdf $<
	mv $(patsubst ../build/uml/%.pdf,../uml-source/%.pdf,$@) $@

build/%.pdf: %.tex
	mkdir -p build
	latexmk $(CONT_FLAG) $(LATEX_COMMON_FLAGS) -auxdir=build -outdir=build $<

build/listings/%.pdf: listings/%.tex
	mkdir -p build/listings
	latexmk $(LATEX_COMMON_FLAGS) -auxdir=build/listings -outdir=build/listings $<

clean:
	rm -f *.tmp *.tui *.log *.tuc *.mp *.bbl *.blg *.fls *.idx *.aux *.out *.fdb_latexmk *.ilg *.ind
	rm -rf build
	rm -rf ../build

latex-force:
	rm -f build/$(PRES_NAME).pdf

cleandev:	clean dev
