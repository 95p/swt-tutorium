import sys
import re
import os
import unicodedata

TOPIC = ''
COUNT = -1


def set_args():
    if len(sys.argv) != 3:
        print('example usage: ./create.py TOPIC 4')
        exit(-1)
    global TOPIC
    global COUNT
    TOPIC = sys.argv[1]
    COUNT = int(sys.argv[2])


def read_questions_from_file():
    with open('res/Multiple_Choice.txt', 'r') as file:
        return tex_escape(file.read())


def parse_questions(data):
    split = data.split('\n\n')

    question_map = {}
    for topic in split:
        under = topic.split('\n')
        question_map[under[0]] = []
        for i in range(1, len(under)):
            right = True
            if under[i][0] == 'f':
                right = False
            under[i] = under[i][2:]
            question_map[under[0]].append([under[i], right])
    return question_map


def format_n(questions, n):
    string = '\\section{Quiz}\n'
    with open('res/template.tex', 'r') as file:
        data = file.read()
    for i in range(0, min(len(questions), n)):
        answer = 'Wahr' if questions[i][1] else 'Falsch'
        string += data.replace('<Q>', questions[i][0]).replace('<A>', answer)
    return string


def slugify(value, allow_unicode=True):
    # This function was largely taken from django (django/django/utils/text.py)
    """
    Methode die sicherstellt, dass keine in Dateinamen nicht erlaubten Zeichen in einem String vorkommen.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower()).strip()
    return re.sub(r'[-\s]+', '-', value)


def tex_escape(text):
    """
        :param text: a plain text message
        :return: the message escaped to appear correctly in LaTeX
    """
    conv = {
        '&': r'\&',
        '%': r'\%',
        '$': r'\$',
        '#': r'\#',
        '_': r'\_',
        '{': r'\{',
        '}': r'\}',
        '~': r'\textasciitilde{}',
        '^': r'\^{}',
        '\\': r'\textbackslash{}',
        '<': r'\textless{}',
        '>': r'\textgreater{}',
        '∞': r'$\infty$'
    }
    regex = re.compile('|'.join(re.escape(str(key)) for key in sorted(conv.keys(), key=lambda item: - len(item))))
    return regex.sub(lambda match: conv[match.group()], text)


def write(to_write, n):
    folder = slugify(TOPIC) + '-Quiz/'
    os.mkdir(folder)
    with open(folder + slugify(TOPIC) + '-Quiz.tex', 'w+') as the_file:
        the_file.write(to_write)


if __name__ == '__main__':
    set_args()
    string = read_questions_from_file()
    parsed = parse_questions(string)
    if TOPIC not in parsed:
        print('No such Topic. Available topics are: ')
        print(parsed.keys())
        exit(-1)
    to_write = format_n(parsed[TOPIC], COUNT)
    write(to_write, TOPIC)
    exit(0)
