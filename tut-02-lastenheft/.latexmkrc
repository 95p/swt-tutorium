$pdf_mode = 1;

$max_repeat = 10;

# See https://tex.stackexchange.com/a/498358
$clean_ext = "synctex.gz nav snm thm soc loc glg acn glg glo gls ist vrb";

add_cus_dep('glo', 'gls', 0, 'makeglo2gls');
sub makeglo2gls {
    system("makeindex -s '$_[0].ist' -t '$_[0]'.glg -o '$_[0]'.gls '$_[0]'.glo");
}
