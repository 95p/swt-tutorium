#!/bin/sh

NAME=$1

cp -r template "$1"
mv "$1/template.tex" "$1/$1.tex"
sed -i.bak "s/MODULENAME/$1/g" "$1/$1.tex"
rm "$1/$1.tex.bak"
